-- Add new stations returned by ServiceDetails. Input list?
-- name: AddStation :many
insert into stations (station_id, station_name)
values (sqlc.arg(station_id)::text, sqlc.arg(station_Name))
on conflict (station_id) do nothing
returning *;

-- Add new service returned by serviceAttributesMetrics
-- name: AddService :exec
insert into services (service_id, origin_station, destination_station)
values (sqlc.arg(service_Id), sqlc.arg(station_Origin)::text, sqlc.arg(station_Destination)::text)
on conflict (service_id) do nothing
returning *;

-- Add new run
-- name: AddRun :exec
insert into runs (rid, service_id)
values (sqlc.arg(rid), sqlc.arg(service_Id))
on conflict (rid) do nothing
returning *;

-- Add new detail
-- name: AddDetail :exec
insert into details (rid, service_id, station, sched_depart, sched_arrival, actual_depart, actual_arrival, delay, late_canc_reason)
values (sqlc.arg(rid), sqlc.arg(service_Id), sqlc.arg(station_Name)::text, sqlc.arg(scheduled_departure), sqlc.arg(scheduled_arrival), sqlc.arg(actual_departure), sqlc.arg(actual_arrival), sqlc.arg(delay_mins), sqlc.arg(late_cancel_reason))
returning *;

-- List services
-- name: ListServices :many
select * from services;

-- List stations
-- name: ListStations :many
select * from stations;

-- List runs
-- name: ListRuns :many
select * from runs order by rid;

-- List details
-- name: ListDetails :many
select * from details order by rid;

-- List details by rids
-- name: ListDetailsByRIDs :many
select * from details where rid = ANY(sqlc.arg(rid)::bigint[]) order by rid;

-- List unique rids in details
-- name: ListDetailsUniqueRIDs :many
select distinct rid from details order by rid;

---- List all Details which match input: datetime range and stations
---- name: ListDetails :many
--select * from details
--where (sched_arrival between sqlc.arg(time_Min) and sqlc.arg(time_Max)) 
--and (station = sqlc.arg(station1)::text) 
--or (station = sqlc.arg(station2)::text);