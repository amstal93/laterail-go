package util

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"go.opentelemetry.io/otel/exporters/prometheus"
	"go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/metric/global"
	"go.opentelemetry.io/otel/metric/instrument"
	"go.opentelemetry.io/otel/metric/instrument/syncint64"
	"go.opentelemetry.io/otel/sdk/metric/aggregator/histogram"
	controller "go.opentelemetry.io/otel/sdk/metric/controller/basic"
	"go.opentelemetry.io/otel/sdk/metric/export/aggregation"
	processor "go.opentelemetry.io/otel/sdk/metric/processor/basic"
	selector "go.opentelemetry.io/otel/sdk/metric/selector/simple"
)

func InitMetric() (*prometheus.Exporter, error) {
	config := prometheus.Config{
		DefaultHistogramBoundaries: []float64{1, 2, 5, 10, 20, 50},
	}
	ctrl := controller.New(
		processor.NewFactory(
			selector.NewWithHistogramDistribution(
				histogram.WithExplicitBoundaries(config.DefaultHistogramBoundaries),
			),
			aggregation.CumulativeTemporalitySelector(),
			processor.WithMemory(true),
		),
	)
	exporter, err := prometheus.New(config, ctrl)
	if err != nil {
		return nil, fmt.Errorf("failed to create Prometheus exporter: %w", err)
	}
	global.SetMeterProvider(exporter.MeterProvider())

	return exporter, nil
}

// Handler to export otel metrics to Prometheus
func PrometheusHandler(exporter *prometheus.Exporter) gin.HandlerFunc {
	return func(c *gin.Context) {
		exporter.ServeHTTP(c.Writer, c.Request)
	}
}

// Middleware to increment otel metrics counters
func CounterMiddleware(counter syncint64.Counter) gin.HandlerFunc {
	return func(c *gin.Context) {
		counter.Add(c, 1)
	}
}

// Create otel metric counter for laterail.api.submit endpoint
func CounterSubmit(meter metric.Meter) syncint64.Counter {
	counter, err := meter.SyncInt64().Counter(
		"laterail.api.count_submit",
		instrument.WithDescription("Counts number of requests to /submit endpoint"),
	)
	if err != nil {
		panic(err)
	}
	return counter
}
