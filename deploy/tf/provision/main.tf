terraform {
  # backend "azurerm" {}
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.87"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "~> 2.12.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.8.0"
    }
  }
  required_version = ">= 1.0.0"
}

provider "gitlab" {
  token = var.gitlab_token
}
provider "azurerm" {
  features {}
}
provider "azuread" {
  tenant_id = var.azure_tenant_id
}

variable "project_name" {
  type    = string
  default = "laterail"
}
variable "location" {
  type    = string
  default = "uksouth"
}
variable "azure_tenant_id" {
  type = string
}
variable "ssh_public_key" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}
variable "migration_file_paths" {
  type = string
  default = "../migrations"
}
variable "resource_groups" {
  type = map(string)
  default = {
    main    = "laterail-main"
    cluster = "laterail-cluster"
  }
}
variable "cluster_name" {
  type    = string
  default = "cluster0"
}
variable "node_pool" {
  type = any
  default = {
    name   = "default"
    number = 3
    size   = "Standard_B2s"
  }
}
variable "secrets_set" {
  type = any
  sensitive   = true
}
variable "stage" {
  type    = string
  default = "prod"
}
variable "gitlab_token" {
  type      = string
  sensitive = true
}

# Used by resources which require unique names
resource "random_integer" "this" {
  min = 1
  max = 9999
}

resource "azurerm_resource_group" "this" {
  name     = "${var.resource_groups.main}-${var.stage}-${random_integer.this.result}"
  location = var.location

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}

resource "azurerm_log_analytics_workspace" "this" {
  name                = "${var.project_name}-workspace-${random_integer.this.result}"
  resource_group_name = azurerm_resource_group.this.name
  location            = azurerm_resource_group.this.location
  sku                 = "PerGB2018"

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}

resource "azurerm_log_analytics_solution" "this" {
  solution_name         = "ContainerInsights"
  workspace_resource_id = azurerm_log_analytics_workspace.this.id
  workspace_name        = azurerm_log_analytics_workspace.this.name
  location              = azurerm_resource_group.this.location
  resource_group_name   = azurerm_resource_group.this.name

  plan {
    publisher = "Microsoft"
    product   = "OMSGallery/ContainerInsights"
  }

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}

# Azure Storage Account File Share used as Persistent Volume
resource "azurerm_storage_account" "this" {
  name                      = "${var.project_name}migration${random_integer.this.result}"
  resource_group_name       = azurerm_kubernetes_cluster.this.node_resource_group
  location                  = azurerm_resource_group.this.location
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
}

resource "azurerm_storage_share" "this" {
  name                 = "migrations"
  storage_account_name = azurerm_storage_account.this.name
  quota                = 1
}

# Upload files used in initial setup to PV
resource "azurerm_storage_share_file" "this" {
  for_each = fileset("${path.root}/${var.migration_file_paths}", "*")

  name             = each.value
  storage_share_id = azurerm_storage_share.this.id
  source           = "${var.migration_file_paths}/${each.value}"

  lifecycle {
    ignore_changes = [
      metadata
    ]
  }
}

# Key Vault and service principal for K8s external-secrets
data "azuread_client_config" "this" {}

resource "azuread_application" "this" {
  display_name = "${var.project_name}-${var.stage}"
  owners       = [data.azuread_client_config.this.object_id]
}

resource "azuread_service_principal" "this" {
  application_id               = azuread_application.this.application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.this.object_id]
}

resource "azuread_service_principal_password" "this" {
  service_principal_id = azuread_service_principal.this.object_id
}

resource "azurerm_key_vault" "this" {
  name                        = "${var.project_name}-${var.stage}"
  location                    = azurerm_resource_group.this.location
  resource_group_name         = azurerm_kubernetes_cluster.this.node_resource_group
  enabled_for_disk_encryption = true
  tenant_id                   = data.azuread_client_config.this.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false

  sku_name = "standard"

  access_policy {
    tenant_id = data.azuread_client_config.this.tenant_id
    object_id = data.azuread_client_config.this.object_id

    secret_permissions = [
      "Backup",
      "Delete",
      "Get",
      "List",
      "Purge",
      "Recover",
      "Restore",
      "Set"
    ]
  }

  access_policy {
    tenant_id = data.azuread_client_config.this.tenant_id
    object_id = azuread_service_principal.this.object_id

    secret_permissions = [
      "Get",
      "List",
    ]
  }
}

# Store new service principal secret in new az key vault
resource "azurerm_key_vault_secret" "sp" {
  name         = "external-secret-service-principal-clientsecret"
  value        = azuread_service_principal_password.this.value
  key_vault_id = azurerm_key_vault.this.id
}

# Add k8s app secrets to key vault
# Use non/sensitive functions to prevent dumping secret values to stdout
# Secrets are stored as base64 encoded json - [{"name": value, "secret": value}]
resource "azurerm_key_vault_secret" "this" {
  for_each = { for s in toset(jsondecode(base64decode(nonsensitive(var.secrets_set)))) : s.name => s }

  name         = each.value.name
  value        = sensitive(each.value.secret)
  key_vault_id = azurerm_key_vault.this.id
}

resource "azurerm_kubernetes_cluster" "this" {
  name                = "${var.stage}-${random_integer.this.result}"
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  dns_prefix          = var.resource_groups.cluster
  node_resource_group = "${var.resource_groups.cluster}-${var.stage}-${random_integer.this.result}"

  default_node_pool {
    name                   = var.node_pool.name
    vm_size                = var.node_pool.size
    os_disk_size_gb        = 30
    node_count             = var.node_pool.number
    # min_count              = 1
    # max_count              = 5
    # enable_auto_scaling    = true
    enable_host_encryption = true
  }

  # linux_profile {
  #   admin_username = "ubuntu"
  #   ssh_key {
  #     key_data = file(var.ssh_public_key)
  #   }
  # }

  identity {
    type = "SystemAssigned"
  }

  network_profile {
    network_plugin    = "kubenet"
    load_balancer_sku = "basic"
  }

  addon_profile {
    oms_agent {
      enabled                    = true
      log_analytics_workspace_id = azurerm_log_analytics_workspace.this.id
    }
  }

  # open_service_mesh {
  #   enabled = true
  # }

  lifecycle {
    ignore_changes = [
      # default_node_pool["node_count"],
      tags
    ]
  }
}

# Add kube_config as variable to gitlab project
resource "gitlab_project_variable" "this" {
    project = "28689390"  #https://gitlab.com/alexchadwick/laterail-go
    key = "kube_config_${var.stage}"
    value = base64encode(azurerm_kubernetes_cluster.this.kube_config_raw) # encoding to support variable masking
    masked = true
}

output "storage_account" {
  value = azurerm_storage_account.this
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.this.kube_config.0
}

output "kube_config_raw" {
  value = azurerm_kubernetes_cluster.this.kube_config_raw
}

output "azuread_service_principal_appid" {
  value = azuread_application.this.application_id
}

output "azuread_service_principal_objectid" {
  value = azuread_service_principal.this.object_id
}

output "azuread_service_principal_password" {
  value = azuread_service_principal_password.this.value
}