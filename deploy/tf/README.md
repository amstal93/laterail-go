# Requirements

- Terraform deploys VMs with [Encryption at host](https://docs.microsoft.com/en-us/azure/virtual-machines/disk-encryption#encryption-at-host---end-to-end-encryption-for-your-vm-data) e2e encryption, which requires you enable the feature EncryptionAtHost per azure subscription:
  `az feature register --namespace Microsoft.Compute --name EncryptionAtHost`
- An azure service principal is used to authenticate terraform provisioning within gitlab-ci pipelines. This service principal requires API role permissions:
  - [General for managing AzureAD with terraform](https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/guides/service_principal_configuration#method-1-api-roles-recommended-for-service-principals)
  - [Extras for managing application and service principal objects](https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/resources/application#api-permissions)

## Environmental variables
Create env vars for terraform authentication against providers. Use `deploy/tf/.env-example` as a reference

## Secret tfvars
gitlab_token
secrets_set

# Terraform cmds
Using the cli on a workstation to delpoy config using dev tfvars, backend files and a secret tfvars file which is not commited to the repo:

*Create `deploy/tf/secrets.tfvars` using `secrets-example.tfvars` as a reference

Working dir == `/deploy/tf` 
```
terraform fmt
(optional - remote backend) terraform init -backend-config env/backend-test
terraform validate
terraform plan -out test.tfplan -var-file env/test.tfvars -var-file env/test.secrets.tfvars
terraform apply test.tfplan
terraform destroy -var-file env/test.tfvars -var-file env/test.secrets.tfvars
```

# Connect kubectl

`az aks get-credentials --resource-group laterail-main --name cluster0`

# Managed K8s and Terraform

## Re-provisioning AKS cluster and dependent resources with the same name shortly after deleting them results in managed identity authorisation errors

During testing I destroyed and immediately redeployed AKS using Terraform and often found that LoadBalancer services would get stuck in stage Pending, reporting authorisation errors against identity objectIDs which didn't exist. E.g.

  ```Error syncing load balancer: failed to ensure load balancer: Retriable: false,
  RetryAfter: 0s, HTTPStatusCode: 403, RawError: Retriable: false, RetryAfter:
  0s, HTTPStatusCode: 403, RawError:
  {"error":{"code":"AuthorizationFailed","message":"The client
  'fefb6daa-6466-45f7-90d8-0114ce7f8ab9' with object id
  'fefb6daa-6466-45f7-90d8-0114ce7f8ab9' does not have authorization to perform
  action 'Microsoft.Network/loadBalancers/read' over scope
  '/subscriptions/56768381-7d01-4ba8-9a04-b3d7f8531b85/resourceGroups/laterail-cluster-test/providers/Microsoft.Network'
  or the scope is invalid. If access was recently granted, please refresh your
  credentials."}}
  ```

The AKS cluster resource also showed Resource Health alerts, e.g.
  ```Degraded : Invalid Service Principal (Customer Initiated)
  At Saturday, December 18, 2021, 3:41:52 PM GMT, the Azure monitoring system received the following information regarding your Azure Kubernetes Service (AKS):
  Your service principal has expired or is invalid. Please check if you are using the correct secret or if the key has expired.
  Recommended Steps
  ```

My investigation got as far as confirming that I was using a managed system identity with AKS and unsuccessfully trying to find the objectids noted in the event errors.

- The error appears to be avoided by creating the AKS cluster in Terraform with a random int appended to the name

## Expired cloud service auth tokens for managed K8s services need to be fetched before you can initialise providers which use them ([tf docs ref](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs#exec-plugins)).

Workaround: [Refresh tokens in tf state using a tageted `terraform apply`](https://github.com/hashicorp/terraform-provider-kubernetes/blob/main/_examples/aks/README.md#kubeconfig-for-manual-cli-access)

```
terraform plan -target=module.provision.azurerm_kubernetes_cluster.this -var-file env/dev.tfvars -var-file env/dev.secrets.tfvars -out aks-renew.tfplan
terraform apply "aks-renew.tfplan"
```

## Re-provisioning Azure Keyvault and dependent resources after deleting while re-using the previous name will recover the destroyed vault

This can result in terraform errors like:

```
│ Error: creating Vault: (Name "laterail-test" / Resource Group "laterail-cluster-test-2442"): keyvault.VaultsClient#CreateOrUpdate: Failure sending request: StatusCode=400 -- Original Error: Code="SoftDeletedVaultDoesNotExist" Message="A soft deleted vault with the given name does not exist. Ensure that the name for the vault that is being attempted to recover is in a recoverable state. For more information on soft delete please follow this link https://go.microsoft.com/fwlink/?linkid=2149745"
│ 
│   with module.provision.azurerm_key_vault.this,
│   on provision/main.tf line 179, in resource "azurerm_key_vault" "this":
│  179: resource "azurerm_key_vault" "this" {
│ 
╵
╷
│ Error: POST https://gitlab.com/api/v4/projects/28689390/variables: 400 {message: {key: [(kube_config_test) has already been taken]}}
│ 
│   with module.provision.gitlab_project_variable.this,
│   on provision/main.tf line 289, in resource "gitlab_project_variable" "this":
│  289: resource "gitlab_project_variable" "this" {
│ 
```
Resolve error by purging the deleted vault resource. Process using azure cli:

`az keyvault list-deleted`
`az keyvault purge --name <name-of-deleted-keyvault>`