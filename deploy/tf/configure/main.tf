terraform {
  # backend "azurerm" {}
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.87"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~>2.7.0"
    }
  }
  required_version = ">= 1.0.0"
}

provider "azurerm" {
  features {}
}

variable "kube_config" {
  type = any
}
variable "kube_config_raw" {
  type = any
}

# Pull kube_config from output of provision module
provider "kubernetes" {
  host = var.kube_config.host

  client_certificate     = base64decode(var.kube_config.client_certificate)
  client_key             = base64decode(var.kube_config.client_key)
  cluster_ca_certificate = base64decode(var.kube_config.cluster_ca_certificate)
}

variable "resource_groups" {
  type = map(string)
  default = {
    main    = "laterail-main"
    cluster = "laterail-cluster"
  }
}
variable "cluster_name" {
  type    = string
  default = "cluster0"
}
variable "app_namespace" {
  type    = string
  default = "laterail"
}
variable "storage_account" {
  type    = any
}
variable "secret_service_principal" {
  type = any
}

# Intentionally using null_resource to avoid state management issue. Will be destroyed with AKS cluster
resource "null_resource" "namespace" {
  provisioner "local-exec" {
    command = <<EOT
      kubectl create namespace $APP_NAMESPACE --kubeconfig <(echo $KUBECONFIG | base64 -d)
    EOT

    interpreter = ["/bin/bash", "-c"]
    environment = {
      KUBECONFIG = base64encode(nonsensitive(var.kube_config_raw)),
      APP_NAMESPACE = var.app_namespace
    }
  }
}

# Store Azure Storage Account access key as K8s secret - auth for pv
resource "kubernetes_secret" "storage" {
  metadata {
    name      = "azure-storage-secret"
    namespace = var.app_namespace
  }

  data = {
    azurestorageaccountname = var.storage_account.name
    azurestorageaccountkey  = var.storage_account.primary_access_key
  }

  depends_on = [
    null_resource.namespace
  ]
}

# Add azure service principal with access to az key vault secrets as K8s secret - auth for external-secrets operator resources
resource "kubernetes_secret" "secret_service_principal" {
  metadata {
    name      = "azure-secret-sp"
    namespace = var.app_namespace
  }

  data = {
    clientid = var.secret_service_principal.clientid
    clientsecret  = var.secret_service_principal.clientsecret
  }

  depends_on = [
    null_resource.namespace
  ]
}

# Intentionally using null_resource to avoid state management issue. Will be destroyed with AKS cluster
# Bootstraps new cluster w/ ArgoCD
resource "null_resource" "kubectl" {
  provisioner "local-exec" {
    command = <<EOT
      kubectl create namespace argocd --kubeconfig <(echo $KUBECONFIG | base64 -d)
      kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj-labs/applicationset/v0.2.0/manifests/install-with-argo-cd.yaml --kubeconfig <(echo $KUBECONFIG | base64 -d)
      kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}' --kubeconfig <(echo $KUBECONFIG | base64 -d)
      kubectl annotate svc argocd-server -n argocd external-dns.alpha.kubernetes.io/hostname="argocd.laterail.com" --kubeconfig <(echo $KUBECONFIG | base64 -d)
    EOT

    interpreter = ["/bin/bash", "-c"]
    environment = {
      KUBECONFIG = base64encode(nonsensitive(var.kube_config_raw))
    }
  }

  depends_on = [
    null_resource.namespace
  ]
}