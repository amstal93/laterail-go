#!/bin/bash
<< COMMENT
Create KinD cluster for local testing
COMMENT
set -e

# Add cluster
cat <<EOF | kind create cluster --wait 30s --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role:  worker
  extraMounts:
  - hostPath: /home/alex/git/laterail-go/deploy/migrations
    containerPath: /mnt/migrations
EOF

# Setup argocd w/ ApplicationSet controller
kubectl create namespace argocd
kubectl apply --namespace argocd -f https://raw.githubusercontent.com/argoproj-labs/applicationset/v0.2.0/manifests/install-with-argo-cd.yaml
kubectl wait --for=condition=Ready pods --all --namespace argocd --timeout=300s
kubectl port-forward svc/argocd-server --namespace argocd 8080:443 &> /dev/null &

# K8s 1.23 (latest) adds feature to wait for jsonpath. Will replace the `until` below
until kubectl --namespace argocd get secret | grep "argocd-initial-admin-secret"; do : ; done
ARGO_PASS=$(kubectl --namespace argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
#argocd login localhost:8080 --username admin --password $ARGO_PASS --insecure

# MetalLB install
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/metallb.yaml

# Configure using a subset of the kind bridge network adaptor range (e.g. 172.19.0.1/16)
# Get current range: docker network inspect -f '{{.IPAM.Config}}' kind
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 172.19.255.1-172.19.255.250
EOF

# Create secrets from env vars
kubectl create namespace laterail
kubectl create secret generic azure-secret-sp --from-literal clientid=${SERVICE_PRINCIPAL_CLIENT_ID} --from-literal clientsecret=${SERVICE_PRINCIPAL_CLIENT_SECRET} --namespace laterail
kubectl create secret generic cloudflare --from-literal API_TOKEN=${CLOUDFLARE_API_TOKEN} --namespace laterail
kubectl create secret generic azure-storage-secret --from-literal azurestorageaccountname=${STORAGE_NAME} --from-literal azurestorageaccountkey=${STORAGE_KEY} --namespace laterail

# Assign IP to argocd service
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'

# Apply dev stage Kustomization
sed -i "s|REPLACE_STAGE|dev|g" ../k8s/root.yaml
sed -i "s|REPLACE_REF|$(git branch --show-current)|g" ../k8s/root.yaml
sed -i "s|REPLACE_API|dev|g" ../k8s/root.yaml
sed -i "s|REPLACE_WEB|dev|g" ../k8s/root.yaml

# Deploy ApplicationSet
kubectl apply -n argocd -f ../k8s/root.yaml
echo $ARGO_PASS